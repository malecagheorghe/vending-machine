package models.money;

public enum Coins {
    Coin200(200, "coinOf2"), Coin100(100, "coinOf1"), Coin50(50, "halfBill"), Coin25(25, "quarter"), Coin10(10, "dime"), Coin5(5, "nickel"), Coin2(2, "twoPennies"), Coin1(1, "penny");

    Coins(int value, String key) {
        this.value = value;
        this.key = key;
    }

    private int value;
    private String key;
    private int limit = 100;

    public int getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
