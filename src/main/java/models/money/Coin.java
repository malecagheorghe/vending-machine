package models.money;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import models.excel.ITreasuryDataXCELL;
import models.excel.TreasuryDataXCELL;

import java.io.IOException;

public class Coin implements Treasury {
    private int value;

    public Coin(int value) {
        this.value = value;
    }

    @Override
    public void makePayment(int money) throws IOException, ItemNotFoundException, TreasuryFullException, TreasuryEmptyException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        String key = treasury.getCoinKey(money);
        int limit = treasury.getLimit(key);
        int quantity = treasury.getQuantity(key);
        int insertedMoney = treasury.getInsertedMoney();
        if (quantity < limit) {
            treasury.setInsertedMoney(insertedMoney + money);
            treasury.increaseCurrencyUnitQuantity(key);
        } else {
            throw new TreasuryFullException();
        }
    }
}
