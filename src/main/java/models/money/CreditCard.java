package models.money;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryFullException;
import models.excel.ITreasuryDataXCELL;
import models.excel.TreasuryDataXCELL;

import java.io.IOException;

public class CreditCard implements Treasury {
    @Override
    public void makePayment(int money) throws IOException, ItemNotFoundException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        int insertedMoney = treasury.getInsertedMoney();
        treasury.setInsertedMoney(insertedMoney + money);
    }
}
