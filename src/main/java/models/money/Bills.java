package models.money;

public enum Bills {
    Bill100(100, "billOf100"), Bill50(50, "billOf50"), Bill20(20, "billOf20"), Bill10(10, "billOf10"), Bill5(5, "billOf5"), Bill2(2, "billOf2"), Bill1(1, "billOf1");

    Bills(int value, String key) {
        this.value = value;
        this.key = key;
    }

    private int value;
    private String key;
    private int limit = 100;

    public int getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
