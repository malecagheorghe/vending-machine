package models.money;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import models.excel.ITreasuryDataXCELL;
import models.excel.TreasuryDataXCELL;

import java.io.IOException;

public class Bill implements Treasury {
    private int value;

    public Bill(int value) {
        this.value = value * 100;
    }

    @Override
    public void makePayment(int money) throws IOException, ItemNotFoundException, TreasuryFullException, TreasuryEmptyException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        String key = treasury.getBillKey(money);
        int limit = treasury.getLimit(key);
        int quantity = treasury.getQuantity(key);
        int insertedMoney = treasury.getInsertedMoney();
        if (quantity < limit) {
            treasury.increaseCurrencyUnitQuantity(key);
            treasury.setInsertedMoney(insertedMoney + money * 100);
        } else {
            throw new TreasuryFullException();
        }
    }

}
