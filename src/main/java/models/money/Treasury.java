package models.money;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;

import java.io.IOException;

public interface Treasury {
    void makePayment(int money) throws IOException, ItemNotFoundException, TreasuryFullException, TreasuryEmptyException;
}
