package models.money;

import models.excel.ITreasuryDataXCELL;
import models.excel.TreasuryDataXCELL;

import java.io.IOException;

public class OnlinePayments implements Treasury {
    @Override
    public void makePayment(int money) throws IOException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        int insertedMoney = treasury.getInsertedMoney();
        treasury.setInsertedMoney(insertedMoney + money);
    }
}
