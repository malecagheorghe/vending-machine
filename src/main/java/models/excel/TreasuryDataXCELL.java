package models.excel;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import models.money.Bills;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SINGLETON
 */
public class TreasuryDataXCELL extends Xcell implements ITreasuryDataXCELL {
    private static String treasuryData = "Treasury Data";
    private static String insertedMoney = "Inserted Money";
    private static TreasuryDataXCELL INSTANCE = new TreasuryDataXCELL("src/main/java/models/excel/TreasuryData.xlsx");

    private TreasuryDataXCELL(String path) {
        super(path);
    }

    public static TreasuryDataXCELL getINSTANCE() {
        return INSTANCE;
    }

    /**
     * to be ran once when the program is first executed
     */{
        try {
            createTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void createTables() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = createTable(workbook, treasuryData, "keys", "values", "storage limit", "quantity");
        createTreasuryTable(sheet);

        sheet = createTable(workbook, insertedMoney, "inserted money");
        createCell(0, 1, 0, sheet);
        saveFile(workbook);
        closeWorkbook(workbook);
    }

    public int getInsertedMoney() throws IOException {
        Cell cell = getCell(insertedMoney, 1, 0);
        int result = (int) cell.getNumericCellValue();
        closeWorkbook(cell);
        return result;
    }

    public List<String> getKeys() throws IOException {
        List<String> result = new ArrayList<>();
        Sheet sheet = getSheet(treasuryData);
        for (Row row : sheet) {
            String key = row.getCell(0).getStringCellValue();
            if (key == null) break;
            else if (!key.equalsIgnoreCase("keys")) {
                result.add(key);
            }
        }
        closeWorkbook(sheet);
        return result;
    }

    public String getBillKey(int currencyValue) throws IOException, ItemNotFoundException {
        Sheet sheet = getSheet(treasuryData);
        for (Row row : sheet) {
            if (row.getRowNum() > 0) {
                Cell cell = row.getCell(1);
                String key = row.getCell(0).getStringCellValue();
                if (currencyValue == (int) cell.getNumericCellValue() && key.startsWith("bill")) {
                    closeWorkbook(sheet);
                    return key;
                }
            }
        }
        closeWorkbook(sheet);
        throw new ItemNotFoundException();
    }

    public String getCoinKey(int currencyValue) throws IOException, ItemNotFoundException {
        Sheet sheet = getSheet(treasuryData);
        for (Row row : sheet) {
            if (row.getRowNum() > 0) {
                Cell cell = row.getCell(1);
                String key = row.getCell(0).getStringCellValue();
                if (currencyValue == (int) cell.getNumericCellValue() && !key.startsWith("bill")) {
                    closeWorkbook(sheet);
                    return key;
                }
            }
        }
        closeWorkbook(sheet);
        throw new ItemNotFoundException();
    }

    public int getValue(String key) throws IOException, ItemNotFoundException {
        Row row = getRowFromKey(this.getSheet(treasuryData), key);
        int result = (int) row.getCell(1).getNumericCellValue();
        closeWorkbook(row);
        return result;
    }

    public int getLimit(String key) throws ItemNotFoundException, IOException {
        Row row = getRowFromKey(this.getSheet(treasuryData), key);
        int result = (int) row.getCell(2).getNumericCellValue();
        closeWorkbook(row);
        return result;
    }

    public int getQuantity(String key) throws ItemNotFoundException, IOException {
        Row row = getRowFromKey(this.getSheet(treasuryData), key);
        int result = (int) row.getCell(3).getNumericCellValue();
        closeWorkbook(row);
        return result;
    }

    public void setInsertedMoney(int money) throws IOException {
        Cell cell = getCell(insertedMoney, 1, 0);
        cell.setCellValue(money);
        saveFile(cell.getSheet().getWorkbook());
        closeWorkbook(cell);
    }

    public void increaseCurrencyUnitQuantity(String key) throws ItemNotFoundException, IOException, TreasuryFullException, TreasuryEmptyException {
        updateCurrencyQuantity(key, 1);
    }

    public void decreaseCurrencyUnitQuantity(String key) throws ItemNotFoundException, IOException, TreasuryEmptyException, TreasuryFullException {
        updateCurrencyQuantity(key, -1);
    }

    public void updateCurrencyQuantity(String key, int amount) throws IOException, ItemNotFoundException, TreasuryEmptyException, TreasuryFullException {
        Row row = getRowFromKey(this.getSheet(treasuryData), key);
        Cell cell = row.getCell(3);
        if (amount > 0 && (getLimit(key) == getQuantity(key))) {
            closeWorkbook(row);
            throw new TreasuryFullException("There is no more space for " + key);
        } else if (getQuantity(key) == 0) {
            closeWorkbook(row);
            throw new TreasuryEmptyException("There are no more " + key);
        }
        cell.setCellValue(cell.getNumericCellValue() + amount);
        saveFile(row.getSheet().getWorkbook());
        closeWorkbook(row);
    }

    public void setCurrencyUnitLimit(String key, int limit) throws ItemNotFoundException, IOException {
        Row row = getRowFromKey(this.getSheet(treasuryData), key);
        Cell cell = row.getCell(2);
        cell.setCellValue(limit);
        saveFile(row.getSheet().getWorkbook());
        closeWorkbook(row);
    }

    public void emptyTreasury() throws IOException {
        Sheet sheet = getSheet(treasuryData);
        for (Row row : sheet) {
            row.getCell(3).setCellValue(0);
        }
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    private void createTreasuryTable(Sheet treasuryData) {
        int rowCounter = 0;
        for (Bills bill : Bills.values()) {
            createCell(0, rowCounter, bill.getKey(), treasuryData);
            createCell(0, rowCounter, bill.getValue(), treasuryData);
            createCell(0, rowCounter, bill.getLimit(), treasuryData);
            createCell(0, rowCounter, 0, treasuryData);//amount
            rowCounter++;
        }
    }

}
