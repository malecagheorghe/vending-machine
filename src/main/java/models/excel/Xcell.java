package models.excel;

import controllers.exceptions.ItemNotFoundException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Xcell {
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public Workbook openFile() throws IOException {
        FileInputStream file = new FileInputStream(new File(this.getFilePath()));
        return new XSSFWorkbook(file);
    }

    public Xcell(String filePath) {
        this.filePath = filePath;
    }

    public Sheet createTable(Workbook workbook, String name, String... fields) throws IOException {
        Sheet sheet = workbook.createSheet(name);
        createHeader(sheet, fields);
        return sheet;
    }

    public List<String> getKeys(String tableName) throws IOException {
        List<String> result = new ArrayList<>();
        Sheet sheet = getSheet(tableName);
        for (Row row : sheet) {
            String key = row.getCell(0).getStringCellValue();
            if (key == null) break;
            else if (!key.equalsIgnoreCase("keys")) {
                result.add(key);
            }
        }
        closeWorkbook(sheet);
        return result;
    }

    public void createHeader(Sheet sheet, String... names) {
        for (int i = 0; i < names.length; i++) {
            createHeaderCell(i, names[i], sheet);
        }
    }

    public Cell createHeaderCell(int index, String name, Sheet sheet) {
        Cell cell = createCell(index, 0, name, sheet);
        cell.setCellStyle(createHeaderCellStyle(sheet.getWorkbook()));
        return cell;
    }

    public CellStyle createHeaderCellStyle(Workbook workbook) {
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        return headerStyle;
    }

    public Cell createCell(int cellIndex, int rowIndex, String name, Sheet sheet) {
        Row row = sheet.createRow(rowIndex);
        Cell cell = row.createCell(cellIndex);
        cell.setCellValue(name);
        return cell;
    }

    public Cell createCell(int cellIndex, int rowIndex, int value, Sheet sheet) {
        Row row = sheet.createRow(rowIndex);
        Cell cell = row.createCell(cellIndex);
        cell.setCellValue(value);
        return cell;
    }

    public void saveFile(Workbook workbook) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(this.filePath);
        workbook.write(outputStream);
        outputStream.close();
    }

    public Cell getCell(String sheetName, int rowIndex, int colIndex) throws IOException {
        return getRow(sheetName, rowIndex).getCell(colIndex);
    }

    public Row getRow(String sheetName, int rowIndex) throws IOException {
        return getSheet(sheetName).getRow(rowIndex);
    }

    public Row getRowFromKey(Sheet sheet, String key) throws ItemNotFoundException, IOException {
        for (Row row : sheet) {
            if (row.getCell(0).getStringCellValue().equalsIgnoreCase(key)) {
                return row;
            }
        }
        throw new ItemNotFoundException();
    }

    public Sheet getSheet(String sheetName) throws IOException {
        FileInputStream file = new FileInputStream(new File(this.filePath));
        Workbook workbook = new XSSFWorkbook(file);
        return workbook.getSheet(sheetName);
    }

    public void closeWorkbook(Cell cell) throws IOException {
        closeWorkbook(cell.getSheet());
    }

    public void closeWorkbook(Row row) throws IOException {
        closeWorkbook(row.getSheet());
    }

    public void closeWorkbook(Sheet sheet) throws IOException {
        closeWorkbook(sheet.getWorkbook());
    }

    public void closeWorkbook(Workbook workbook) throws IOException {
        workbook.close();
    }
}
