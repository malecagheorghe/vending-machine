package models.excel;

import controllers.exceptions.ItemNotFoundException;
import controllers.hardware.servoMotors.ServoMotor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductStorageDataXCELL extends Xcell implements IVendingStorageData {
    private static ProductStorageDataXCELL INSTANCE = new ProductStorageDataXCELL("src/main/java/models/excel/ProductStorageData.xlsx");

    private ProductStorageDataXCELL(String path) {
        super(path);
    }

    public static ProductStorageDataXCELL getINSTANCE() {
        return INSTANCE;
    }

    private static String productLine = "ProductLine";
    private static String itemInfo = "ItemInfo";

    /**
     * to be ran once when the program is first executed
     */ {
        try {
            createTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createTables() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        createTable(workbook, productLine, "productCode", "servoMotorID");
        createTable(workbook, itemInfo, "productCode", "name", "price", "quantity");
        saveFile(workbook);
        closeWorkbook(workbook);
    }

    public void addProduct(String productCode, String name, int price, int quantity, int... servoMotorID) throws IOException {
        Sheet sheet = openFile().getSheet(itemInfo);
        try {
            getRowFromKey(sheet, productCode);
            removeProductLine(productCode);
        } catch (ItemNotFoundException ignored) {
        } finally {
            for (int key : servoMotorID) {
                setProductLine(productCode, key);
            }
        }
        setItem(productCode, name, price, quantity);
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    private void setItem(String productCode, String name, int price, int quantity) throws IOException {
        Sheet sheet = this.openFile().getSheet(itemInfo);
        try {
            Row row = getRowFromKey(sheet, productCode);
            row.getCell(1).setCellValue(name);
            row.getCell(2).setCellValue(price);
            row.getCell(3).setCellValue(quantity);
        } catch (ItemNotFoundException e) {
            int rowIndex = sheet.getPhysicalNumberOfRows();
            createCell(0, rowIndex, productCode, sheet);
            createCell(1, rowIndex, name, sheet);
            createCell(2, rowIndex, price, sheet);
            createCell(3, rowIndex, quantity, sheet);
        }
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    private void removeProductLine(String productCode) throws IOException {
        Sheet sheet = this.openFile().getSheet(productLine);
        for (Row row : sheet) {
            if (row.getCell(0).getStringCellValue().equalsIgnoreCase(productCode)) sheet.removeRow(row);
        }
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    private void setProductLine(String productCode, int... servoMotorsCodes) throws IOException {
        Sheet sheet = this.openFile().getSheet(productLine);
        int rowIndex = sheet.getPhysicalNumberOfRows();
        for (int key : servoMotorsCodes) {
            createCell(0, rowIndex, productCode, sheet);
            createCell(1, rowIndex, key, sheet);
            rowIndex++;
        }
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    public List<Integer> getProductLineMotors(String productCode) throws IOException {
        Sheet sheet = this.openFile().getSheet(productLine);
        List<Integer> result = new ArrayList<>();
        for (Row row : sheet) {
            if (row.getCell(0).getStringCellValue().equalsIgnoreCase(productCode)) {
                result.add((int) row.getCell(1).getNumericCellValue());
            }
        }
        closeWorkbook(sheet);
        return result;
    }

    public void removeProduct(String productCode) throws IOException, ItemNotFoundException {
        Workbook workbook = openFile();
        Sheet sheet = workbook.getSheet(itemInfo);
        Row row = getRowFromKey(sheet, productCode);
        sheet.removeRow(row);
        sheet = workbook.getSheet(productLine);
        for (Row r : sheet) {
            if (r.getCell(0).getStringCellValue().equalsIgnoreCase(productCode)) sheet.removeRow(r);
        }
        saveFile(workbook);
        closeWorkbook(workbook);
    }

    public String getProductName(String productCode) throws IOException, ItemNotFoundException {
        return getProductInfo(productCode);
    }

    public int getProductQuantity(String productCode) throws IOException, ItemNotFoundException {
        return getProductInfo(productCode, "quantity");
    }

    public int getProductPrice(String productCode) throws IOException, ItemNotFoundException {
        return getProductInfo(productCode, "price");
    }

    private int getProductInfo(String productCode, String field) throws ItemNotFoundException, IOException {
        int result;
        Sheet sheet = openFile().getSheet(itemInfo);
        switch (field) {
            case "price":
                result = (int) getRowFromKey(sheet, productCode).getCell(2).getNumericCellValue();
                break;
            case "quantity":
                result = (int) getRowFromKey(sheet, productCode).getCell(3).getNumericCellValue();
                break;
            default:
                throw new ItemNotFoundException();
        }
        closeWorkbook(sheet);
        return result;
    }

    private String getProductInfo(String productCode) throws IOException, ItemNotFoundException {
        Sheet sheet = openFile().getSheet(itemInfo);
        String result = getRowFromKey(sheet, productCode).getCell(1).getStringCellValue();
        closeWorkbook(sheet);
        return result;
    }

    @Override
    public void pushProduct(String productCode) throws IOException, ItemNotFoundException {
        List<Integer> motors = getProductLineMotors(productCode);
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        treasury.setInsertedMoney(treasury.getInsertedMoney() - this.getProductPrice(productCode));
        for (int motorID : motors) {
            new ServoMotor(motorID).push();
        }
    }

    @Override
    public void pullProduct(String productCode) throws IOException {
        List<Integer> motors = getProductLineMotors(productCode);
        for (int motorID : motors) {
            new ServoMotor(motorID).pull();
        }
    }

    @Override
    public void increaseProductQuantity(String productCode) throws IOException, ItemNotFoundException {
        changeProductQuantity(productCode, 1);
    }

    @Override
    public void decreaseProductQuantity(String productCode) throws IOException, ItemNotFoundException {
        changeProductQuantity(productCode, -1);
    }

    public void changeProductQuantity(String productCode, int quantity) throws IOException, ItemNotFoundException {
        Sheet sheet = openFile().getSheet(itemInfo);
        getRowFromKey(sheet, productCode).getCell(3).setCellValue(quantity);
        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }
}
