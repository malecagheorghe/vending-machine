package models.excel;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;

import java.io.IOException;
import java.util.List;

public interface ITreasuryDataXCELL {
    void createTables() throws IOException;

    void setInsertedMoney(int money) throws IOException;

    int getInsertedMoney() throws IOException;

    List<String> getKeys() throws IOException;

    String getBillKey(int currencyValue) throws IOException, ItemNotFoundException;

    String getCoinKey(int currencyValue) throws IOException, ItemNotFoundException;

    int getValue(String key) throws IOException, ItemNotFoundException;

    int getLimit(String key) throws ItemNotFoundException, IOException;

    int getQuantity(String key) throws ItemNotFoundException, IOException;

    void increaseCurrencyUnitQuantity(String key) throws ItemNotFoundException, IOException, TreasuryFullException, TreasuryEmptyException;

    void decreaseCurrencyUnitQuantity(String key) throws ItemNotFoundException, IOException, TreasuryEmptyException, TreasuryFullException;

    void setCurrencyUnitLimit(String key, int limit) throws ItemNotFoundException, IOException;

    void emptyTreasury() throws IOException;

}