package models.excel;

import controllers.exceptions.ItemNotFoundException;

import java.io.IOException;

public interface IVendingStorageData {
    void addProduct(String productCode, String name, int price, int quantity, int... servoMotorID) throws IOException;

    void removeProduct(String productCode) throws IOException, ItemNotFoundException;

    String getProductName(String productCode) throws IOException, ItemNotFoundException;

    int getProductQuantity(String productCode) throws IOException, ItemNotFoundException;

    int getProductPrice(String productCode) throws IOException, ItemNotFoundException;

    void pushProduct(String productCode) throws IOException, ItemNotFoundException;

    void pullProduct(String productCode) throws IOException;

    void increaseProductQuantity(String productCode) throws IOException, ItemNotFoundException;

    void decreaseProductQuantity(String productCode) throws IOException, ItemNotFoundException;
}
