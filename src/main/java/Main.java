import controllers.IVendingMachine;
import controllers.VendingMachine;
import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import controllers.hardware.sensors.SensorBills;
import controllers.hardware.sensors.Sensors;
import models.money.Bills;
import models.money.Coins;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        IVendingMachine vendingMachine = new VendingMachine();

        try {
            vendingMachine.addNewItem("String productCode", "String productName", 100.1, 12, 1, 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.removeItem("String productCode");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.refillMachine("String productCode", 5);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.refillTreasury(Bills.Bill50.getKey());
        } catch (TreasuryEmptyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        } catch (TreasuryFullException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.setProductQuantity("String productCode", 11);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.setTreasury(Coins.Coin50.getKey(), 5);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.emptyTreasury();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.buyProduct("String productCode");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        try {
            vendingMachine.insertMoney(new SensorBills(), 5);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        } catch (TreasuryEmptyException e) {
            e.printStackTrace();
        } catch (TreasuryFullException e) {
            e.printStackTrace();
        }
        try {
            vendingMachine.giveRest();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
