package controllers.hardware.servoMotors;

public class ServoMotor implements Motor {
    private Motor motor;

    public ServoMotor(int id) {
        if (id % 2 == 0) {
            motor = new CounterClockwise(id);
        } else {
            motor = new Clockwise(id);
        }
    }

    @Override
    public void push() {
        new Thread(() -> motor.push()).start();
    }

    @Override
    public void pull() {
        new Thread(() -> motor.pull()).start();
    }

}
