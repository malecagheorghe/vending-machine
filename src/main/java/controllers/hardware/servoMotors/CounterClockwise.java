package controllers.hardware.servoMotors;

public class CounterClockwise implements Motor {
    private int id;

    public CounterClockwise(int id) {
        this.id = id;
    }

    @Override
    public void push() {
        //code which would activate an electric motor
        System.out.println("Rotating counterclockwise motor " + id);
    }

    @Override
    public void pull() {
        //code which would activate an electric motor
        System.out.println("Rotating clockwise motor " + id);
    }
}
