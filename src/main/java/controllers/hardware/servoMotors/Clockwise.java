package controllers.hardware.servoMotors;

public class Clockwise implements Motor {
    private int id;

    public Clockwise(int id) {
        this.id = id;
    }

    @Override
    public void push() {
        //code which would activate an electric motor
        System.out.println("Rotating clockwise motor " + id);
    }

    @Override
    public void pull() {
        //code which would activate an electric motor
        System.out.println("Rotating counterclockwise motor " + id);
    }
}
