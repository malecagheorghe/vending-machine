package controllers.hardware.servoMotors;

public interface Motor {
    void push();
    void pull();
}
