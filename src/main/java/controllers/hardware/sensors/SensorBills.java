package controllers.hardware.sensors;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryFullException;
import models.money.Bill;
import models.money.CreditCard;
import models.money.Treasury;

import java.io.IOException;

public class SensorBills implements Sensors {
    @Override
    public void activateSensor(int money) throws IOException, ItemNotFoundException, TreasuryFullException {
        Treasury currency = new Bill(money);
        currency.makePayment(money);
    }
}
