package controllers.hardware.sensors;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import models.money.Coin;
import models.money.CreditCard;
import models.money.Treasury;

import java.io.IOException;

public class SensorCoins implements Sensors {
    @Override
    public void activateSensor(int money) throws IOException, ItemNotFoundException, TreasuryFullException, TreasuryEmptyException {
        Treasury currency = new Coin(money);
        currency.makePayment(money);
    }
}
