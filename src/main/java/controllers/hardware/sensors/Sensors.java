package controllers.hardware.sensors;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;

import java.io.IOException;

public interface Sensors {
    void activateSensor(int money) throws IOException, ItemNotFoundException, TreasuryEmptyException, TreasuryFullException;
}
