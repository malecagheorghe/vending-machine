package controllers.hardware.sensors;

import controllers.exceptions.ItemNotFoundException;
import models.money.CreditCard;
import models.money.Treasury;

import java.io.IOException;

public class SensorCreditCard implements Sensors {
    @Override
    public void activateSensor(int money) throws IOException, ItemNotFoundException {
        Treasury currency = new CreditCard();
        currency.makePayment(money);
    }
}
