package controllers;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import controllers.hardware.sensors.Sensors;
import models.excel.ITreasuryDataXCELL;
import models.excel.IVendingStorageData;
import models.excel.ProductStorageDataXCELL;
import models.excel.TreasuryDataXCELL;

import java.io.IOException;
import java.util.List;

public class VendingMachine implements IVendingMachine {
    @Override
    public void buyProduct(String productCode) throws Throwable {
        IVendingStorageData products = ProductStorageDataXCELL.getINSTANCE();
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        int price = products.getProductPrice(productCode);
        while (price > treasury.getInsertedMoney()) {
            System.out.println(price);
        }
        if (checkRest()) {
            products.pushProduct(productCode);
            giveRest();
        }
    }

    @Override
    public void giveRest() throws IOException, ItemNotFoundException, TreasuryFullException, TreasuryEmptyException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        List<String> keys = treasury.getKeys();

        for (String key : keys) {
            int insertedMoney = treasury.getInsertedMoney();
            int currentValue = treasury.getValue(key);
            while (currentValue <= insertedMoney && treasury.getQuantity(key) > 0) {
                returnMoney(key);
            }
        }
    }

    public boolean checkRest() throws IOException, ItemNotFoundException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        List<String> keys = treasury.getKeys();

        int insertedMoney = treasury.getInsertedMoney();
        int counter = 0;

        for (String key : keys) {
            int currentValue = treasury.getValue(key);
            int amount = treasury.getQuantity(key);
            while (currentValue <= insertedMoney) {
                if (insertedMoney == 0) return true;
                if (amount > 0) {
                    amount--;
                    insertedMoney -= currentValue;
                } else break;
            }
        }

        return false;

    }

    @Override
    public void emptyTreasury() throws IOException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        treasury.emptyTreasury();
    }

    @Override
    public void refillTreasury(String currency) throws TreasuryFullException, IOException, ItemNotFoundException, TreasuryEmptyException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        treasury.increaseCurrencyUnitQuantity(currency);
    }

    @Override
    public void setTreasury(String currency, int quantity) throws IOException, ItemNotFoundException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        for (int i = 0; i < quantity; i++) {
            try {
                treasury.increaseCurrencyUnitQuantity(currency);
            } catch (Exception e) {
                treasury.setCurrencyUnitLimit(currency, quantity);
            }
        }
    }

    @Override
    public void refillMachine(String productCode, int quantity) throws IOException, ItemNotFoundException {
        IVendingStorageData storage = ProductStorageDataXCELL.getINSTANCE();
        for (int i = 0; i < quantity; i++) {
            storage.pullProduct(productCode);
            storage.increaseProductQuantity(productCode);
        }
    }

    @Override
    public void setProductQuantity(String productCode, int quantity) throws IOException, ItemNotFoundException {
        IVendingStorageData storage = ProductStorageDataXCELL.getINSTANCE();
        for (int i = 0; i < quantity; i++) {
            storage.increaseProductQuantity(productCode);
        }
    }

    @Override
    public void addNewItem(String productCode, String productName, Double productPrice, int amountOfProducts, int... lineId) throws IOException {
        IVendingStorageData storage = ProductStorageDataXCELL.getINSTANCE();
        storage.addProduct(productCode, productName, (int) (productPrice * 100), amountOfProducts, lineId);
    }

    @Override
    public void removeItem(String productCode) throws IOException, ItemNotFoundException {
        IVendingStorageData storage = ProductStorageDataXCELL.getINSTANCE();
        storage.removeProduct(productCode);
    }

    @Override
    public void insertMoney(Sensors sensor, int money) throws IOException, ItemNotFoundException, TreasuryEmptyException, TreasuryFullException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        String key;
        try {
            key = treasury.getBillKey(money);
        } catch (ItemNotFoundException e) {
            key = treasury.getCoinKey(money);
        }
        try {
            sensor.activateSensor(money);
        } catch (TreasuryFullException e) {
            returnMoney(key);
        }
    }

    @Override
    public void returnMoney(String key) throws TreasuryEmptyException, IOException, ItemNotFoundException, TreasuryFullException {
        ITreasuryDataXCELL treasury = TreasuryDataXCELL.getINSTANCE();
        treasury.decreaseCurrencyUnitQuantity(key);
        treasury.setInsertedMoney(treasury.getInsertedMoney() - treasury.getValue(key));
        //a method which would expel the currency out from the machine
        System.out.println(key + "returned");
    }

}
