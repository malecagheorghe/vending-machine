package controllers;

import controllers.exceptions.ItemNotFoundException;
import controllers.exceptions.TreasuryEmptyException;
import controllers.exceptions.TreasuryFullException;
import controllers.hardware.sensors.Sensors;

import java.io.IOException;

/**
 * This interface defines the functionality the users/maintenance personnel have when interacting with the machine
 */
public interface IVendingMachine {
    /**
     * parameters: the product code inserted by the user
     * return: void
     * functionality: This method takes the user input and ejects the product after enough money were inserted. In case there is no change available the machine ejects the money back.
     */
    void buyProduct(String productCode) throws Throwable;

    /**
     * parameters: none
     * return: void
     * functionality: ejects the rest. Money counter - product price = rest to be ejected; (recursively return the biggest currency unit that fits in the argument)
     */
    void giveRest() throws Exception;

    /**
     * parameters: currency
     * return: void
     * functionality: ejects all the chosen currency
     */
    void emptyTreasury() throws IOException;

    /**
     * parameters: [int] the value on the coin/ bill
     * return: void
     * functionality: gets the input (one coin/ bill at a time) and stores it in the treasury
     */
    void refillTreasury(String currency) throws TreasuryEmptyException, IOException, ItemNotFoundException, TreasuryFullException;

    /**
     * parameters: the product code the maintenance personal wants to refill (one at a time)
     * return: void
     * functionality: This method gets the product and puts it in the machine. If there is no more space, the machine throws exception.
     */
    void refillMachine(String productCode, int quantity) throws IOException, ItemNotFoundException;

    /**
     * parameters: the amount of lines (1/2) for a product. In case the product is bigger, it will take two lines/springs to host it.
     * return: void
     * functionality: This function allocates the required amount of lines/ springs per product. If not possible => throws exception.
     */
    void addNewItem(String productCode, String productName, Double productPrice, int amountOfProducts, int... lineId) throws IOException;

    void removeItem(String productCode) throws IOException, ItemNotFoundException;

    void insertMoney(Sensors sensor, int money) throws IOException, ItemNotFoundException, TreasuryEmptyException, TreasuryFullException;

    void returnMoney(String key) throws TreasuryEmptyException, IOException, ItemNotFoundException, TreasuryFullException;

    void setTreasury(String currency, int quantity) throws IOException, ItemNotFoundException;

    void setProductQuantity(String productCode, int quantity) throws IOException, ItemNotFoundException;

}
