package controllers.exceptions;

public class NotSufficientChangeException extends Exception {
    public NotSufficientChangeException() {
    }

    public NotSufficientChangeException(String message) {
        super(message);
    }
}
