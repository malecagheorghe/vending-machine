package controllers.exceptions;

public class TreasuryEmptyException extends Exception {
    public TreasuryEmptyException() {
    }

    public TreasuryEmptyException(String message) {
        super(message);
    }
}
