package controllers.exceptions;

public class WrongFileStreamSpecificationException extends Exception {
    public WrongFileStreamSpecificationException() {
    }

    public WrongFileStreamSpecificationException(String message) {
        super(message);
    }
}
