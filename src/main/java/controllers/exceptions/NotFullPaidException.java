package controllers.exceptions;

public class NotFullPaidException extends Exception {
    public NotFullPaidException() {
    }

    public NotFullPaidException(String message) {
        super(message);
    }
}
