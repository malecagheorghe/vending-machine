package controllers.exceptions;

public class WrongCurrencyException extends Exception {
    public WrongCurrencyException() {
    }

    public WrongCurrencyException(String message) {
        super(message);
    }
}
