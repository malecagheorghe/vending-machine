package controllers.exceptions;

public class TreasuryFullException extends Exception {
    public TreasuryFullException() {
    }

    public TreasuryFullException(String message) {
        super(message);
    }
}
